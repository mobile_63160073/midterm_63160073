import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  String? str = stdin.readLineSync();

  //1. Tokenizing String
  RegExp regExp = new RegExp(r'(\d+)|([+ \- * / ^ %])|([( )])');
  var token = new List<String?>.filled(0, "", growable: true);
  Iterable<RegExpMatch> match = regExp.allMatches(str!);
  for(final i in match){
    token.add(i[0]);
  }
  var infix = new List<String>.filled(0, "", growable: true);
  for(final i in token){
    infix.add(i![0]);
  }
  print("Infix: $infix");

  // toPostfix
  var tokenPostfix = toPostfix(infix);
  var postfix = new List<String>.filled(0, "", growable: true);
  for(final i in tokenPostfix){
    postfix.add(i![0]);
  }
  print("Postfix: $postfix");

  // Calculate
  var result = calPostfix(postfix);
  print("Result of Postfix: $result");
}

//2. Infix to Postfix
List<String?> toPostfix(List<String> infix){
  var operators = new List<String?>.filled(0, "", growable: true);
  var postfix = new List<String?>.filled(0, "", growable: true);

  for(final i in infix){
    if(i[0].contains(RegExp(r'(\d+)'))){
      postfix.add(i[0]);
      continue;
    }
    if(i[0].contains(RegExp(r'([+ \- * / ^ %])'))){
      while(operators.isNotEmpty 
        && !operators.last!.contains('(') 
        && ((i[0].contains(RegExp(r'[+ \-]')) && operators.last!.contains(RegExp(r'[* / % ^]')))
        || (i[0].contains(RegExp(r'[+ \- * / %]')) && operators.last!.contains('^')))){
          postfix.add(operators.removeLast());
      }
      operators.add(i[0]);
    }
    if(i[0].contains('(')){
      operators.add(i[0]);
    }
    if(i[0].contains(')')){
      while(!operators.last!.contains('(')){
        postfix.add(operators.removeLast());
      }
      operators.remove('(');
    }
  }

  while(operators.isNotEmpty){
    postfix.add(operators.removeLast());
  }
  return postfix;
}

//3. Evaluate Postfix
int calPostfix(List<String> postfix){
  var values = new List<int>.filled(0, 0, growable: true);

  for(final i in postfix){
    if(i[0].contains(RegExp(r'(\d+)'))){
      values.add(int.parse(i[0]));
    }else{
      int right = values.removeLast();
      int left = values.removeLast();
      int result = 0;
      switch(i[0]){
        case '+': {
          result = left+right;
        }
        break;
        case '-': {
          result = left-right;
        }
        break;
        case '*': {
          result = left*right;
        }
        break;
        case '/': {
          result = left~/right;
        }
        break;
        case '%': {
          result = left%right;
        }
        break;
        case '^': {
          result = pow(left, right)~/1;
        }
        break;
      }
      values.add(result);
    }  
  }
  return values.first;
}